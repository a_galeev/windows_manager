#include "core.h"



MyBrowser::MyBrowser(int X, int Y, int W, int H, const char* L)
	: Fl_Browser(X, Y, W, H, L)
{
	dragitem = 0; dragged = 0;
}

void MyBrowser::change_cursor(Fl_Cursor newcursor)
{
	fl_cursor(newcursor, FL_BLACK, FL_WHITE);
}

void* MyBrowser::item_under_mouse()
{
	int X, Y, W, H; bbox(X, Y, W, H);
	if (!Fl::event_inside(X, Y, W, H))
		return 0;       // for scrollbar
	return find_item(Fl::event_y());
}

int MyBrowser::handle(int event)
{
	switch (event)
	{
	case FL_PUSH:
	{
		// See if left mouse click might be starting an item drag..
		int modifiers = FL_SHIFT | FL_CTRL | FL_ALT | FL_META;
		if (Fl::event_button() &&             // left button?
			(!(Fl::event_state() & modifiers)) // no modifiers?
			) {
			if ((dragitem = item_under_mouse()))
			{  // find item clicked on
				Fl_Browser_::select_only(dragitem, 0);   // select item (for visibility)
				change_cursor(FL_CURSOR_HAND);          // change cursor
				dragged = 0;
				return(1);    // prevent Fl_Browser from seeing event
			}
		}
		break;
	}
	case FL_DRAG:
	{
		if (dragitem)
		{
			void *dst_item = item_under_mouse();
			if (dst_item != dragitem)
			{
				item_swap(dragitem, dst_item);
				Fl_Browser_::select(dragitem, 1, 0);  // (keeps focus box on dragged item)
				dragged = 1;
				redraw();
			}
			return(1);    // prevent Fl_Browser from seeing event
		}
		break;
	}
	case FL_RELEASE:
	{
		if (dragitem)
		{
			change_cursor(FL_CURSOR_DEFAULT);       // ensure normal cursor
			if (dragged)
			{
				Fl_Browser_::select(dragitem, 0, 0);    // deselect dragged item
				redraw();
			}

			dragitem = 0;                           // disable item drag mode
			dragged = 0;
			return(1);    // prevent Fl_Browser from seeing event
		}
		break;
	}
	}
	return Fl_Browser::handle(event);
}

static MyBrowser* brow;
static int counter;
vector<shared_ptr<string>> v_sp;
vector<shared_ptr<BaseWin>> v_wp;


BaseWin::BaseWin(int X, int Y, int W, int H, const char* T = (const char*)0)
	: Fl_Window(X, Y, W, H, T) {}


	void WinManager::raw_exit(BaseWin* q, void* v)
	{
		exit(0);
	}


	void WinManager::close_cb(BaseWin* q, void* v)
	{
		for (int i = 1; i <= brow->size(); ++i)
			if (string(brow->text(i)) == static_cast<string*>(v)->c_str())
				brow->remove(i);
		brow->redraw();
		Fl::delete_widget(q);
	}
	void WinManager::add_cb(Fl_Button *theButton, void*)
	{
		counter++;
		v_sp.push_back(shared_ptr<string>(new string("Window " + to_string(counter))));
		v_wp.push_back(shared_ptr<BaseWin>(new BaseWin(std_top_left_dot, std_top_left_dot, std_win_length, std_win_length, v_sp.back()->c_str())));
		v_wp.back()->callback((Fl_Callback*)close_cb, static_cast<void*>(v_sp.back().get()));
		brow->add(v_sp.back()->c_str(), v_wp.back().get());
		v_wp.back()->show();
	}
	void WinManager::red_cb(Fl_Button *theButton, void*d)
	{

		for (int i = 1; i <= brow->size(); ++i)
		{

			((BaseWin*)brow->data(i))->show();
		}

	}
	WinManager::WinManager()
	{
		counter = 0;
		brow = nullptr;
		win = shared_ptr<BaseWin>(new BaseWin((Fl::w() - std_win_length) / 2, (Fl::h() - std_win_length) / 2, std_win_length, std_win_length));	//create main window in the center of a screen

		win->begin();

		win->callback(*(Fl_Callback*)raw_exit);	//callback for exit button

		add = shared_ptr< Fl_Button>(new Fl_Button(3 * std_button_sz, 2 * std_button_sz, std_button_sz, std_button_sz, "New window"));	//callback for adding window button
		add->tooltip("Make new window");
		add->callback((Fl_Callback*)add_cb);

		red = shared_ptr<Fl_Button>(new Fl_Button(4 * std_button_sz, 2 * std_button_sz, std_button_sz, std_button_sz, "Redraw"));	//callback for redraw button
		red->tooltip("Redraw windows according to priority list on the left");
		red->callback((Fl_Callback*)red_cb);

		brow = new MyBrowser(std_top_left_dot, std_top_left_dot, std_win_length / 3, win->h() - 5 * std_top_left_dot, "Windows Draw Priority");	//create list of opened windows
		

		win->end();
		win->show();
	}
	WinManager::~WinManager()
	{
		v_sp.clear();
		v_wp.clear();
		delete brow;
	}


