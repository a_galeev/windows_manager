#pragma once

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Browser.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <memory>
#include <iostream>	//for cerr
#include <vector>
#include <string>	//for windows name et. al.
#include <exception>

using namespace std;


class MyBrowser : public Fl_Browser
{
	void *dragitem;
	char dragged;
	void change_cursor(Fl_Cursor);
	void *item_under_mouse();
	int handle(int event);
public:
	MyBrowser(int, int, int, int, const char*);
};

struct BaseWin :public Fl_Window
{
	BaseWin(int, int, int, int, const char*);
};

class WinManager
{
	static const int std_win_length = 600;
	static const int std_top_left_dot = 10;
	static const int std_button_sz = 100;

	shared_ptr<BaseWin> win;
	shared_ptr<Fl_Button> add;
	shared_ptr<Fl_Button> red;


	static void raw_exit(BaseWin*, void*);
	static void close_cb(BaseWin*, void*);
	static void add_cb(Fl_Button*, void*);
	static void red_cb(Fl_Button*, void*);

public:

	WinManager();
	~WinManager();

};


