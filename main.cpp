#include "core.h"

int main(int argc, char **argv) 
{
	try
	{
		WinManager mng;
		return Fl::run();
	}

	catch (exception& e)
	{
		cerr << "Error: " << e.what() << endl;
		return 1;
	}

	
}